/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import com.lastminute.interview.selmi.receipts.ReceiptPrinter;
import com.lastminute.interview.selmi.taxes.TaxCalculator;
import com.lastminute.interview.selmi.taxes.TaxCalculatorFactory;
import java.math.BigDecimal;
import java.util.UUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class SimpleShoppingBasketTest {

    @Mock
    private TaxCalculatorFactory factory;
    @Mock
    private TaxCalculator calculator;
    @Mock
    private ReceiptPrinter printer;
    @Mock
    private Product product;
    
    private ShoppingBasket basket;

    @Before
    public void setup() {
        basket = new SimpleShoppingBasket(factory, printer);
        when(factory.get(any(Product.class))).thenReturn(calculator);
        when(calculator.get(any(BigDecimal.class))).thenReturn(BigDecimal.ONE);
        when(product.getId()).thenReturn(UUID.randomUUID());
        when(product.getPrice()).thenReturn(BigDecimal.TEN);
    }

    @Test
    public void newlyCreatedBasketHasNoItemsAndPricesAreZero() {
        assertTrue(basket.getItems().isEmpty());
        assertEquals(BigDecimal.ZERO, basket.geTotalTaxes());
        assertEquals(BigDecimal.ZERO, basket.getTotalPrice());
    }
    
    @Test
    public void addShouldAddToItemsListAndUpdatePrices(){
        basket.add(product);
        assertEquals(BigDecimal.valueOf(11), basket.getTotalPrice());
        assertEquals(BigDecimal.ONE, basket.geTotalTaxes());
        assertEquals(1, basket.getItems().size());
        assertFalse(basket.getItems().isEmpty());
        
    }
    
    @Test
    public void removeShouldRemoveFromItemsListAndUpdatePrices(){
        basket.add(product);
        basket.remove(product);
        assertEquals(BigDecimal.ZERO, basket.getTotalPrice());
        assertEquals(BigDecimal.ZERO, basket.geTotalTaxes());
        assertTrue(basket.getItems().isEmpty());
    }
    
    @Test(expected = ShoppingBasket.InvalidProductException.class)
    public void addOrRemoveOfInvalidProductShouldThrowException(){
        basket.add(null);
    }
    
    @Test
    public void printShouldPrintReceiptUsingReceiptPrinter(){
        basket.add(product);
        basket.printReceipt();
        verify(printer).print(basket);
    }

}
