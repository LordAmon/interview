/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import com.lastminute.interview.selmi.taxes.TaxCalculator;
import java.math.BigDecimal;
import java.util.UUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class SimpleBasketItemTest {

    @Mock
    private TaxCalculator taxCalculator;

    private Product product;

    private UUID productId;

    private BasketItem item;

    @Before
    public void setup() {
        productId = UUID.randomUUID();
        product = createProductMock(productId);
        when(taxCalculator.get(any(BigDecimal.class))).thenReturn(BigDecimal.ONE);
        item = new SimpleBasketItem(taxCalculator, product);
    }

    @Test
    public void getIdShouldReturnTheProductId() {
        assertEquals(product.getId(), item.getId());
    }

    @Test
    public void getDescriptionShouldReturnTheProductDescription() {
        assertEquals(product.getDescription(), item.getDescription());
    }

    @Test
    public void getPriceShouldReturnTheProductPrice() {
        assertEquals(product.getPrice(), item.getPrice());
    }

    @Test
    public void getCategoryShouldReturnTheProductCategory() {
        assertEquals(product.getCategory(), item.getCategory());
    }

    @Test
    public void getTypeShouldReturnTheProductType() {
        assertEquals(product.getType(), item.getType());
    }
    
    @Test
    public void amounOfNewlyCreatedBasketItemMustBeOne(){
        assertEquals(1, item.getAmount());
    }
    
    @Test
    public void totalPriceShouldBeTheSumOfTaxesAndPriceMultipliedByTheAmount(){
        item.setAmount(2);
        assertEquals(BigDecimal.valueOf(22), item.getTotalPrice());
    }
    
    @Test
    public void getTaxesShouldReturnTheValueOfTaxCalculatorMultipliedByTheAmount(){
        item.setAmount(3);
        assertEquals(BigDecimal.valueOf(3), item.getTaxes());
    }
    
    @Test
    public void setAmountShouldIncrementAmountAndUpdatePrices(){
        item.setAmount(2);
        assertEquals(2, item.getAmount());
        assertEquals(BigDecimal.valueOf(22), item.getTotalPrice());
        assertEquals(BigDecimal.valueOf(2), item.getTaxes());
    }
    
    @Test
    public void equalsOnNullMustReturnFalse() {
        assertFalse(item.equals(null));
    }

    @Test
    public void equalsOnObjectOfDifferentTypeMustReturnFalse() {
        assertFalse(item.equals(product));
    }

    @Test
    public void equalsOnItemWithDifferentProductMustReturnFalse() {
        assertFalse(item.equals(new SimpleBasketItem(taxCalculator, createProductMock(UUID.randomUUID()))));
    }

    @Test
    public void equalsOnItemWithSameProductMustReturnTrue() {
        assertTrue(item.equals(new SimpleBasketItem(taxCalculator, product)));
    }
    
    @Test
    public void hashcodeOfDifferentItemsMustNotBeTheSame(){
        when(product.getId()).thenReturn(UUID.randomUUID());
        assertNotEquals(item.hashCode(), new SimpleBasketItem(taxCalculator, createProductMock(UUID.randomUUID())).hashCode());
    }
    
    @Test
    public void hashcodeOfIdenticalItemsMustBeTheSame(){
        assertEquals(item.hashCode(), new SimpleBasketItem(taxCalculator, product).hashCode());
    }
    
    private Product createProductMock(UUID prodId) {
        Product mockedProduct = mock(Product.class);
        when(mockedProduct.getId()).thenReturn(prodId);
        when(mockedProduct.getDescription()).thenReturn("product description");
        when(mockedProduct.getPrice()).thenReturn(BigDecimal.TEN);
        when(mockedProduct.getCategory()).thenReturn(ProductCategory.MUSIC);
        when(mockedProduct.getType()).thenReturn(ProductType.STANDARD);
        return mockedProduct;
    }
}
