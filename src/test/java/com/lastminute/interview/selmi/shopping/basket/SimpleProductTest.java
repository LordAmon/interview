/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import java.math.BigDecimal;
import java.util.UUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author amon
 */
public class SimpleProductTest {

    private Product product;
    private UUID id;

    @Before
    public void setup() {
        id = UUID.randomUUID();
        product = SimpleProduct.with(id)
                .with(BigDecimal.TEN)
                .with(BigDecimal.TEN)
                .with(ProductCategory.MEDICALS)
                .with(ProductType.STANDARD)
                .with("Description").build();
    }

    @Test
    public void verifyId() {
        assertEquals(id, product.getId());
    }

    @Test
    public void verifyDescription() {
        assertEquals("Description", product.getDescription());
    }

    @Test
    public void verifyPrice() {
        assertEquals(BigDecimal.TEN, product.getPrice());
    }

    @Test
    public void verifytype() {
        assertEquals(ProductType.STANDARD, product.getType());
    }

    @Test
    public void verifyCategory() {
        assertEquals(ProductCategory.MEDICALS, product.getCategory());
    }

    @Test
    public void equalsOnNullMustReturnFalse() {
        assertFalse(product.equals(null));
    }

    @Test
    public void equalsOnObjectOfDifferentTypeMustReturnFalse() {
        assertFalse(product.equals("different type"));
    }

    @Test
    public void equalsOnProductWithDifferentIdMustReturnFalse() {
        assertFalse(product.equals(SimpleProduct.with(UUID.randomUUID()).build()));
    }

    @Test
    public void equalsOnProductWithSameIdMustReturnTrue() {
        assertTrue(product.equals(SimpleProduct.with(id).build()));
    }
    
    @Test
    public void hashcodeOfDifferentProductsMustNotBeTheSame(){
        assertNotEquals(product.hashCode(), SimpleProduct.with(UUID.randomUUID()).build().hashCode());
    }
    
    @Test
    public void hashcodeOfIdenticalProductsMustBeTheSame(){
        assertEquals(product.hashCode(), SimpleProduct.with(id).build().hashCode());
    }
    
}
