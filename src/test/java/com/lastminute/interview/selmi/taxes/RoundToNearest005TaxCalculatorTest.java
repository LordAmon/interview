/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class RoundToNearest005TaxCalculatorTest {
    
    @Mock
    private TaxCalculator calculator;
    
    @Test
    public void roundShouldBeToNearest005(){
        final BigDecimal price = BigDecimal.valueOf(47.50);
        when(calculator.get(price)).thenReturn(BigDecimal.valueOf(4.75));
        assertEquals(BigDecimal.valueOf(4.75), new RoundToNearest005TaxCalculator(calculator).get(price));
    }
    
    @Test
    public void roundShouldBeToNearest005Bis(){
        final BigDecimal price = BigDecimal.valueOf(12.99);
        when(calculator.get(price)).thenReturn(BigDecimal.valueOf(1.299));
        assertEquals(BigDecimal.valueOf(1.30).setScale(2), new RoundToNearest005TaxCalculator(calculator).get(price));
    }
    
}
