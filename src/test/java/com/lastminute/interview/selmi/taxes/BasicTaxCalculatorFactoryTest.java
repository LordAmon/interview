/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import com.lastminute.interview.selmi.shopping.basket.Product;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.FOOD;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.MUSIC;
import java.math.BigDecimal;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class BasicTaxCalculatorFactoryTest {
 
    @Mock
    private Product product;
    
    private BasicTaxCalculatorFactory factory;
    
    @Before
    public void setup(){
        factory = new BasicTaxCalculatorFactory(Arrays.asList(FOOD));
        when(product.getPrice()).thenReturn(BigDecimal.valueOf(47.50));
    }
    
    @Test
    public void untaxableCategoryShouldNeverReturnNull(){
        when(product.getCategory()).thenReturn(FOOD);
        assertNotNull(factory.get(product));
    }
    
    @Test
    public void taxableCategoryShouldNeverReturnNull(){
        when(product.getCategory()).thenReturn(MUSIC);
        assertNotNull(factory.get(product));
    }
}
