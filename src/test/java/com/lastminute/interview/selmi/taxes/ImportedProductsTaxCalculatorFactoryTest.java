/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import com.lastminute.interview.selmi.shopping.basket.Product;
import static com.lastminute.interview.selmi.shopping.basket.ProductType.IMPORTED;
import static com.lastminute.interview.selmi.shopping.basket.ProductType.STANDARD;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class ImportedProductsTaxCalculatorFactoryTest {
    @Mock
    private Product product;
    @Mock
    private TaxCalculatorFactory factoryMock;
    @Mock
    private TaxCalculator calculator;
    
    @Mock
    private TaxCalculatorFactory factory;
    @Before
    public void setup(){
        when(factoryMock.get(product)).thenReturn(calculator);
        factory = new ImportedProductsTaxCalculatorFactory(factoryMock);
    }
    
    @Test
    public void nonImportedProductShouldNeverReturnNull(){
        when(product.getType()).thenReturn(STANDARD);
        assertNotNull(factory.get(product));
    }
    
    @Test
    public void importedProductsShouldNeverReturnNull(){
        when(product.getType()).thenReturn(IMPORTED);
        assertNotNull(factory.get(product));
    }
}
