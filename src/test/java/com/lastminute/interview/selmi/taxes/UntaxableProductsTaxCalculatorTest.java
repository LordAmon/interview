/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author amon
 */
public class UntaxableProductsTaxCalculatorTest {
    
    @Test
    public void untaxableProductsTaxCalculatorShouldAlwaysReturnZero(){
        assertEquals(BigDecimal.ZERO.setScale(2), new UntaxableProductsTaxCalculator().get(BigDecimal.valueOf(17.34)));
    }
}
