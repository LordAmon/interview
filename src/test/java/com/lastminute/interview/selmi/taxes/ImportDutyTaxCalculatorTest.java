/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class ImportDutyTaxCalculatorTest {
    private static final int ADDITIONAL_TAX_RATE = 5;
    
    @Mock
    private TaxCalculator unknownTaxCalculator;
    private TaxCalculator calculator;
    
    @Before
    public void setup(){
        calculator = new ImportDutyTaxCalculator(unknownTaxCalculator, ADDITIONAL_TAX_RATE);
    }
    
    @Test
    public void calculatorGetShouldReturnTenPercentOfThePrice(){
        when(unknownTaxCalculator.get(any(BigDecimal.class))).thenReturn(BigDecimal.valueOf(2.80));
        assertEquals(BigDecimal.valueOf(4.1995), calculator.get(BigDecimal.valueOf(27.99)));
        verify(unknownTaxCalculator).get(any(BigDecimal.class));
    }
}
