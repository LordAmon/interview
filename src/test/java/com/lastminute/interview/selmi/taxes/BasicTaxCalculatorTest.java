/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author amon
 */
public class BasicTaxCalculatorTest {
    
    private static final int BASIC_TAX_RATE = 10;
    
    private TaxCalculator calculator;
    
    @Before
    public void setup(){
        calculator = new BasicTaxCalculator(BASIC_TAX_RATE);
    }
    
    @Test
    public void calculatorGetShouldReturnTenPercentOfThePrice(){
        assertEquals(BigDecimal.valueOf(1.499), calculator.get(BigDecimal.valueOf(14.99)));
    }
    
}
