/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.receipts;

import com.lastminute.interview.selmi.shopping.basket.BasketItem;
import com.lastminute.interview.selmi.shopping.basket.ShoppingBasket;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author amon
 */
@RunWith(MockitoJUnitRunner.class)
public class SystemOutReceiptPrinterTest {

    @Mock
    private ShoppingBasket basket;
    @Mock
    private BasketItem item;

    @Mock
    private PrintStream systemOut;
    private ReceiptPrinter printer;

    @Before
    public void setup() {
        System.setOut(systemOut);
        printer = new SystemOutReceiptPrinter();
    }

    @Test
    public void printerShouldPrintAllItems() {
        List<BasketItem> items = Arrays.asList(item, item, item);
        when(basket.getItems()).thenReturn(items);
        printer.print(basket);
        verify(systemOut).println("0 null: null\n"
                + "0 null: null\n"
                + "0 null: null\n"
                + "Sales Taxes: null\n"
                + "Total: null");
    }
}
