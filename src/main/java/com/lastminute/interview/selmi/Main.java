/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi;

import com.lastminute.interview.selmi.receipts.SystemOutReceiptPrinter;
import com.lastminute.interview.selmi.shopping.basket.Product;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.BEAUTY;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.BOOK;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.FOOD;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.MEDICALS;
import static com.lastminute.interview.selmi.shopping.basket.ProductCategory.MUSIC;
import static com.lastminute.interview.selmi.shopping.basket.ProductType.IMPORTED;
import static com.lastminute.interview.selmi.shopping.basket.ProductType.STANDARD;
import com.lastminute.interview.selmi.shopping.basket.ShoppingBasket;
import com.lastminute.interview.selmi.shopping.basket.SimpleProduct;
import com.lastminute.interview.selmi.shopping.basket.SimpleShoppingBasket;
import com.lastminute.interview.selmi.taxes.BasicTaxCalculatorFactory;
import com.lastminute.interview.selmi.taxes.ImportedProductsTaxCalculatorFactory;
import com.lastminute.interview.selmi.taxes.TaxCalculatorFactory;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.UUID;

/**
 *
 * @author amon
 */
public class Main {

    private ShoppingBasket basket;

    public static void main(String[] args) {
        new Main().setup().case1();
        new Main().setup().case2();
        new Main().setup().case3();
    }

    private Main setup() {
        final TaxCalculatorFactory factory = new ImportedProductsTaxCalculatorFactory(new BasicTaxCalculatorFactory(Arrays.asList(BOOK, FOOD, MEDICALS)));
        basket = new SimpleShoppingBasket(factory, new SystemOutReceiptPrinter());
        return this;
    }

    private void case1() {
        System.out.println("\n\n*** Input 1: \n"
                + "1 book at 12.49 \n"
                + "1 music CD at 14.99 \n"
                + "1 chocolate bar at 0.85 ");
        Product prod1 = SimpleProduct.with(UUID.randomUUID())
                .with(BOOK)
                .with("book")
                .with(BigDecimal.valueOf(12.49))
                .with(STANDARD)
                .build();

        Product prod2 = SimpleProduct.with(UUID.randomUUID())
                .with(MUSIC)
                .with("music CD")
                .with(BigDecimal.valueOf(14.99))
                .with(STANDARD)
                .build();

        Product prod3 = SimpleProduct.with(UUID.randomUUID())
                .with(FOOD)
                .with("chocolate bar")
                .with(BigDecimal.valueOf(0.85))
                .with(STANDARD)
                .build();

        basket.add(prod1);
        basket.add(prod2);
        basket.add(prod3);
        System.out.println("*** Output 1");
        basket.printReceipt();
    }

    private void case2() {
        System.out.println("\n\n*** Input 2: \n"
                + "1 imported box of chocolates at 10.00 \n"
                + "1 imported bottle of perfume at 47.50");
        Product prod1 = SimpleProduct.with(UUID.randomUUID())
                .with(FOOD)
                .with("imported box of chocolates")
                .with(BigDecimal.valueOf(10.00))
                .with(IMPORTED)
                .build();

        Product prod2 = SimpleProduct.with(UUID.randomUUID())
                .with(BEAUTY)
                .with("imported bottle of perfume")
                .with(BigDecimal.valueOf(47.50))
                .with(IMPORTED)
                .build();

        basket.add(prod1);
        basket.add(prod2);
        System.out.println("*** Output 2");
        basket.printReceipt();
    }

    private void case3() {
        System.out.println("\n\n*** Input 3: \n"
                + "1 imported bottle of perfume at 27.99 \n"
                + "1 bottle of perfume at 18.99 \n"
                + "1 packet of headache pills at 9.75 \n"
                + "1 box of imported chocolates at 11.25 ");
        Product prod1 = SimpleProduct.with(UUID.randomUUID())
                .with(BEAUTY)
                .with("imported bottle of perfume")
                .with(BigDecimal.valueOf(27.99))
                .with(IMPORTED)
                .build();

        Product prod2 = SimpleProduct.with(UUID.randomUUID())
                .with(BEAUTY)
                .with("bottle of perfume")
                .with(BigDecimal.valueOf(18.99))
                .with(STANDARD)
                .build();

        Product prod3 = SimpleProduct.with(UUID.randomUUID())
                .with(MEDICALS)
                .with("packet of headache pills")
                .with(BigDecimal.valueOf(9.75))
                .with(STANDARD)
                .build();

        Product prod4 = SimpleProduct.with(UUID.randomUUID())
                .with(FOOD)
                .with("box of imported chocolates")
                .with(BigDecimal.valueOf(11.25))
                .with(IMPORTED)
                .build();

        basket.add(prod1);
        basket.add(prod2);
        basket.add(prod3);
        basket.add(prod4);
        System.out.println("*** Output 3");
        basket.printReceipt();
    }
}
