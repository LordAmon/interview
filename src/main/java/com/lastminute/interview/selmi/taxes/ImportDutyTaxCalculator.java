/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author amon
 */
class ImportDutyTaxCalculator implements TaxCalculator {
    
    private final TaxCalculator calculator;
    private final TaxCalculator additionalFivePercentTaxCalculator;
            
    
    ImportDutyTaxCalculator(TaxCalculator calculator, int additionalPercentage){
        this.calculator = calculator;
        additionalFivePercentTaxCalculator = new BasicTaxCalculator(additionalPercentage);
    }

    @Override
    public BigDecimal get(BigDecimal productPrice) {
        return calculator.get(productPrice).add(additionalFivePercentTaxCalculator.get(productPrice));
    }
}
