/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import com.lastminute.interview.selmi.shopping.basket.Product;
import com.lastminute.interview.selmi.shopping.basket.ProductCategory;
import java.util.Collection;

/**
 *
 * @author amon
 */
public class BasicTaxCalculatorFactory implements TaxCalculatorFactory {

    private static final int BASIC_TAX_RATE = 10;

    private final Collection<ProductCategory> untaxableCategories;

    public BasicTaxCalculatorFactory(Collection<ProductCategory> untaxableCategories) {
        this.untaxableCategories = untaxableCategories;
    }

    @Override
    public TaxCalculator get(Product product) {
        return untaxableCategories.contains(product.getCategory()) ? new UntaxableProductsTaxCalculator() : new RoundToNearest005TaxCalculator(new BasicTaxCalculator(BASIC_TAX_RATE));
    }

}
