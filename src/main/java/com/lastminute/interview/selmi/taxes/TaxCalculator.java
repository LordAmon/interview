/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;

/**
 *
 * @author amon
 */
public interface TaxCalculator {
    
    BigDecimal get(BigDecimal productPrice);
    
}
