/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import com.lastminute.interview.selmi.shopping.basket.Product;

/**
 *
 * @author amon
 */
public interface TaxCalculatorFactory {
    
    TaxCalculator get(Product product);
}
