/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author amon
 */
class BasicTaxCalculator implements TaxCalculator {

    private final BigDecimal taxRate;

    public BasicTaxCalculator(int taxRate) {
        this.taxRate = new BigDecimal(taxRate).divide(BigDecimal.valueOf(100.00));
    }

    @Override
    public BigDecimal get(BigDecimal productPrice) {
        return productPrice.multiply(taxRate);
    }

}
