/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import com.lastminute.interview.selmi.shopping.basket.Product;
import static com.lastminute.interview.selmi.shopping.basket.ProductType.IMPORTED;

/**
 *
 * @author amon
 */
public class ImportedProductsTaxCalculatorFactory implements TaxCalculatorFactory{
    
    private static final int ADDITIONAL_PERCENTAGE = 5;
    
    private final TaxCalculatorFactory basicFactory;

    public ImportedProductsTaxCalculatorFactory(TaxCalculatorFactory basicFactory) {
        this.basicFactory = basicFactory;
    }

    @Override
    public TaxCalculator get(Product product) {
        final TaxCalculator calculator = basicFactory.get(product);
        return new RoundToNearest005TaxCalculator(IMPORTED.equals(product.getType()) ? new ImportDutyTaxCalculator(calculator, ADDITIONAL_PERCENTAGE) : calculator);
    }
    
    

}
