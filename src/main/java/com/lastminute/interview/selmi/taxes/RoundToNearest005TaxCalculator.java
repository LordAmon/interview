/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.taxes;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author amon
 */
public class RoundToNearest005TaxCalculator implements TaxCalculator {

    private static final BigDecimal NEAREST_ROUND = BigDecimal.valueOf(0.05);

    private final TaxCalculator calculator;

    public RoundToNearest005TaxCalculator(TaxCalculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public BigDecimal get(BigDecimal productPrice) {
        BigDecimal divided = calculator.get(productPrice).divide(NEAREST_ROUND, 0, RoundingMode.UP);
        return divided.multiply(NEAREST_ROUND);
    }

}
