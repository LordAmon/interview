/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.receipts;

import com.lastminute.interview.selmi.shopping.basket.ShoppingBasket;

/**
 *
 * @author amon
 */
public interface ReceiptPrinter {
    
    void print(ShoppingBasket basket);
}
