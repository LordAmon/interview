/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.receipts;

import com.lastminute.interview.selmi.shopping.basket.ShoppingBasket;
import java.util.stream.Collectors;

/**
 *
 * @author amon
 */
public class SystemOutReceiptPrinter implements ReceiptPrinter {

    @Override
    public void print(ShoppingBasket basket) {
        String detail = basket.getItems().stream().map(item -> item.getAmount() + " " + item.getDescription() + ": " + item.getTotalPrice()).collect(Collectors.joining("\n"));
        detail += "\nSales Taxes: " + basket.geTotalTaxes() + "\n";
        detail += "Total: " + basket.getTotalPrice();
        System.out.println(detail);
    }
    
}
