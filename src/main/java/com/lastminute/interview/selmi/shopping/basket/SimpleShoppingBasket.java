/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import com.lastminute.interview.selmi.receipts.ReceiptPrinter;
import com.lastminute.interview.selmi.taxes.TaxCalculatorFactory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 *
 * @author amon
 */
public class SimpleShoppingBasket implements ShoppingBasket {

    private final TaxCalculatorFactory taxCalculatorsFactory;
    private final ReceiptPrinter receipPrinter;
    private BigDecimal totalTaxes;
    private BigDecimal totalPrice;

    private final List<BasketItem> items;

    public SimpleShoppingBasket(TaxCalculatorFactory taxCalculatorsFactory, ReceiptPrinter receipPrinter) {
        this.taxCalculatorsFactory = taxCalculatorsFactory;
        this.receipPrinter = receipPrinter;
        items = new ArrayList<>();
        this.totalTaxes = BigDecimal.ZERO;
        this.totalPrice = BigDecimal.ZERO;
    }

    @Override
    public List<BasketItem> getItems() {
        return items;
    }

    @Override
    public BigDecimal geTotalTaxes() {
        return totalTaxes;
    }

    @Override
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    @Override
    public void add(Product product) {
        update(product, items::add, 1);
    }

    @Override
    public void remove(Product product) {
        update(product, items::remove, -1);
    }

    @Override
    public void printReceipt() {
        receipPrinter.print(this);
    }

    private void checkProduct(Product product) {
        if (Objects.isNull(product)) {
            throw new InvalidProductException();
        }
    }

    private void update(Product product, Function<BasketItem, Boolean> listModifier, int multiplier) {
        checkProduct(product);
        final BasketItem item = new SimpleBasketItem(taxCalculatorsFactory.get(product), product);
        listModifier.apply(item);
        this.totalTaxes = this.totalTaxes.add(item.getTaxes().multiply(BigDecimal.valueOf(multiplier)));
        this.totalPrice = this.totalPrice.add(item.getTotalPrice().multiply(BigDecimal.valueOf(multiplier)));
    }

}
