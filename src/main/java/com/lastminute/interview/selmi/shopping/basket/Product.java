/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import java.math.BigDecimal;
import java.util.UUID;

/**
 *
 * @author amon
 */
public interface Product {

    UUID getId();

    String getDescription();

    BigDecimal getPrice();

    ProductType getType();

    ProductCategory getCategory();
}
