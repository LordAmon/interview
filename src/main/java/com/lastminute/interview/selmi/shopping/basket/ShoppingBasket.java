/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author amon
 */
public interface ShoppingBasket {

    List<BasketItem> getItems();

    BigDecimal geTotalTaxes();

    BigDecimal getTotalPrice();

    void add(Product product);

    void remove(Product product);

    void printReceipt();

    public static class InvalidProductException extends RuntimeException {
    }
}
