/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import com.lastminute.interview.selmi.taxes.TaxCalculator;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author amon
 */
class SimpleBasketItem implements BasketItem {

    private int amount;
    private BigDecimal taxes;
    private BigDecimal totalPrice;
    private final TaxCalculator taxCalulator;
    private final Product product;

    public SimpleBasketItem(TaxCalculator taxCalulator, Product product) {
        this.taxCalulator = taxCalulator;
        this.product = product;
        this.amount = 1;
        updatePrices();
    }

    @Override
    public int getAmount() {
        return amount;
    }
    
    @Override
    public void setAmount(int amount){
        this.amount = amount;
        updatePrices();
    }

    @Override
    public BigDecimal getTaxes() {
        return taxes;
    }

    @Override
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    @Override
    public UUID getId() {
        return product.getId();
    }

    @Override
    public String getDescription() {
        return product.getDescription();
    }

    @Override
    public BigDecimal getPrice() {
        return product.getPrice();
    }

    @Override
    public ProductType getType() {
        return product.getType();
    }

    @Override
    public ProductCategory getCategory() {
        return product.getCategory();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.product);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleBasketItem other = (SimpleBasketItem) obj;
        return Objects.equals(this.product, other.product);
    }

    private void updatePrices() {
        this.taxes = taxCalulator.get(product.getPrice()).multiply(BigDecimal.valueOf(this.amount));
        this.totalPrice = product.getPrice().multiply(BigDecimal.valueOf(this.amount)).add(this.taxes);
    }

}
