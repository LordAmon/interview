/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lastminute.interview.selmi.shopping.basket;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author amon
 */
public class SimpleProduct implements Product {

    private final UUID id;
    private final String description;
    private final BigDecimal price;
    private final ProductType type;
    private final ProductCategory category;

    private SimpleProduct(ProductBuilder builder) {
        this.id = builder.getId();
        this.description = builder.getDescription();
        this.price = builder.getPrice();
        this.type = builder.getType();
        this.category = builder.getCategory();
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public ProductType getType() {
        return type;
    }

    @Override
    public ProductCategory getCategory() {
        return category;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleProduct other = (SimpleProduct) obj;
        return Objects.equals(this.id, other.id);
    }

    public static ProductBuilder with(UUID id) {
        return new ProductBuilder(id);
    }

    public static class ProductBuilder {

        private final UUID id;
        private String description;
        private BigDecimal price;
        private ProductType type;
        private ProductCategory category;

        private ProductBuilder(UUID id) {
            this.id = id;
        }

        private UUID getId() {
            return id;
        }

        private String getDescription() {
            return description;
        }

        public ProductBuilder with(String description) {
            this.description = description;
            return this;
        }

        private BigDecimal getPrice() {
            return price;
        }

        public ProductBuilder with(BigDecimal price) {
            this.price = price;
            return this;
        }

        private ProductType getType() {
            return type;
        }

        public ProductBuilder with(ProductType type) {
            this.type = type;
            return this;
        }

        private ProductCategory getCategory() {
            return category;
        }

        public ProductBuilder with(ProductCategory category) {
            this.category = category;
            return this;
        }

        public Product build() {
            return new SimpleProduct(this);
        }

    }

}
